/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2015 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2015.seq", ModelFileLocation.REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2015/V6.503");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2015";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2014 optics */
        definitionSetList.add(this.create2015InjectionOpticsSet());
        definitionSetList.add(this.create2015LowBetaOpticsSet());
        definitionSetList.add(this.create2015MediumBetaOpticsSetc());
        definitionSetList.add(this.create2015HighBetaOpticsSet());
        definitionSetList.add(this.create2015IonOpticsTest1Set());
        definitionSetList.add(this.create2015IonOpticsTest3Set());
        definitionSetList.add(this.create2015IonOpticsTest2Set());
        definitionSetList.add(this.create2015LowBetaIonOpticsSet());
        definitionSetList.add(this.create2015SpecialMdOpticsSet());
                  
        return definitionSetList;
    }

    private OpticDefinitionSet create2015InjectionOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic("R2015a_A11mC11mA10mL10m_INJ", new OpticModelFileBuilder[0]);

        return builder.build();
    }

 /**
  * The special optics for MD - alignement ...    
  * @return
  */
    private OpticDefinitionSet create2015SpecialMdOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
       builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj-balistic.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic("R2015a_A11mC11mA10mL10m_ALIGN",                
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/align/alignment_ir1.madx"),
                OpticModelFileBuilder.createInstance("strength/IR5/align/alignment_ir5.madx")} );

        return builder.build();
    }

    private OpticDefinitionSet create2015LowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* optic with coll tunes */
        builder.addOptic("R2015a_A11mC11mA10mL10m",                 
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/align/ir1_9000.madx"),
                OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        /* squeeze sequence down to 0.8/10/0.08/3 */

        builder.addOptic(
                "R2015a_A900C900A10m_0.00950L900_0.00934",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A700C700A10m_0.00950L800_0.00919",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A400C400A10m_0.00950L700_0.00906",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A300C300A10m_0.00950L600_0.00895",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A250C250A10m_0.00950L500_0.00886",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A200C200A10m_0.00950L400_0.00880",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A150C150A10m_0.00950L350_0.00877",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A120C120A10m_0.00950L325_0.00876",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A100C100A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A90C90A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2015a_A80C80A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        /* down to 40 cm */

        builder.addOptic(
                "R2015a_A70C70A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A65C65A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_650.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_650.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A60C60A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A55C55A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_550.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_550.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A50C50A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A45C45A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_450.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_450.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2015a_A40C40A10m_0.00950L300_0.00875",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_400.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_400.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        return builder.build();
    }
 
    
    /**
     * A first model for low beta with IR2 in // to the others
     * @return
     */
    private OpticDefinitionSet create2015IonOpticsTest3Set() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 0.8/0.8/0.8/3 */

        builder.addOptic(
                "R2015t3_A900C900A960L900",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_960.madx") });

        builder.addOptic(
                "R2015t3_A700C700A880L800",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_880.madx") });

        builder.addOptic(
                "R2015t3_A400C400A800L700",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_800.madx") });

        builder.addOptic(
                "R2015t3_A300C300A760L600",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });

        builder.addOptic(
                "R2015t3_A250C250A550L500",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_550.madx") });

        builder.addOptic(
                "R2015t3_A200C200A400L400",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_400.madx") });

        builder.addOptic(
                "R2015t3_A150C150A250L350",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });

        builder.addOptic(
                "R2015t3_A120C120A160L325",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_160.madx") });

        builder.addOptic(
                "R2015t3_A100C100A120L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_120.madx") });

        builder.addOptic(
                "R2015t3_A90C90A100L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_100.madx") });

        builder.addOptic(
                "R2015t3_A80C80A80L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_80.madx") });

 
        return builder.build();
    }

    /**
     * A second model for the beta, with only partial squeeze in parallel
     * @return
     */
    
    private OpticDefinitionSet create2015IonOpticsTest2Set() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 0.8/0.8/0.8/3 */

        builder.addOptic(
                "R2015t2_A700C700A920L800",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_920.madx") });

        builder.addOptic(
                "R2015t2_A400C400A840L700",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_840.madx") });

        builder.addOptic(
                "R2015t2_A300C300A800L600",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_800.madx") });

        builder.addOptic(
                "R2015t2_A250C250A760L500",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });

        builder.addOptic(
                "R2015t2_A200C200A720L400",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_720.madx") });

        builder.addOptic(
                "R2015t2_A150C150A550L350",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_550.madx") });

        builder.addOptic(
                "R2015t2_A120C120A450L325",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_450.madx") });

        builder.addOptic(
                "R2015t2_A100C100A350L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });

        builder.addOptic(
                "R2015t2_A90C90A300L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_300.madx") });

        builder.addOptic(
                "R2015t2_A80C80A250L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });

        builder.addOptic(
                "R2015t2_A80C80A160L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_160.madx") });

        builder.addOptic(
                "R2015t2_A80C80A120L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_120.madx") });

        builder.addOptic(
                "R2015t2_A80C80A100L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_100.madx") });

        builder.addOptic(
                "R2015t2_A80C80A80L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_80.madx") });

 
        return builder.build();
    }
    /**
     * A second model for the beta, with only partial squeeze in parallel
     * @return
     */
    
    private OpticDefinitionSet create2015LowBetaIonOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        
        // pre-squeeze with ALICE to 8 m

        builder.addOptic(
                "R2015i_A11mC11ma960L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_960.madx") });

        builder.addOptic(
                "R2015i_A11mC11mA920L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_920.madx") });

       builder.addOptic(
               "R2015i_A11mC11mA880L10m",
               new OpticModelFileBuilder[] {
                       OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_880.madx") });

       builder.addOptic(
               "R2015i_A11mC11mA840L10m",
               new OpticModelFileBuilder[] {
                       OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_840.madx") });
      
       builder.addOptic(
               "R2015i_A11mC11mA800L10m",
               new OpticModelFileBuilder[] {
                       OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_800.madx") });

        
        /* squeeze sequence down to 0.8/0.8/0.8/3 */
       
        builder.addOptic(
                "R2015i_A900C900A760L900",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });


        builder.addOptic(
                "R2015i_A700C700A720L800",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_720.madx") });

        builder.addOptic(
                "R2015i_A400C400A450L700",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_450.madx") });

        builder.addOptic(
                "R2015i_A300C300A350L600",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });

        builder.addOptic(
                "R2015i_A250C250A250L500",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });

        builder.addOptic(
                "R2015i_A200C200A180L400",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_180.madx") });

        builder.addOptic(
                "R2015i_A150C150A140L350",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_140.madx") });

        builder.addOptic(
                "R2015i_A120C120A120L325",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_120.madx") });

        builder.addOptic(
                "R2015i_A100C100A100L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_100.madx") });

        builder.addOptic(
                "R2015i_A90C90A90L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_90.madx") });

        builder.addOptic(
                "R2015i_A80C80A80L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_80.madx") });



 
        return builder.build();
    }

    private OpticDefinitionSet create2015MediumBetaOpticsSetb() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 20/20/20/20 */

        builder.addOptic(
                "R2015b_A12mC12mA12mL12m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_12000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_12000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_12000_937.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_12000_950.madx") });

        builder.addOptic(
                "R2015b_A14mC14mA14mL14m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_14000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_14000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_14000_923.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_14000_950.madx") });

        builder.addOptic(
                "R2015b_A16mC16mA16mL16m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_16000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_16000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_16000_910.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_16000_950.madx") });

        builder.addOptic(
                "R2015b_A18mC18mA18mL18m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_18000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_18000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_18000_896.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_18000_950.madx") });

        builder.addOptic(
                "R2015b_A20mC20mA20mL20m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_20000_883.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950.madx") });

        /* squeeze sequence down to 30 m in IR8 */

        builder.addOptic(
                "R2015b_A20mC20mA20mL22m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_22000_869.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950.madx") });

        builder.addOptic(
                "R2015b_A20mC20mA20mL26m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_26000_842.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950.madx") });

        builder.addOptic(
                "R2015b_A20mC20mA20mL30m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_30000_815.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950.madx") });

        return builder.build();
    }

    /**
     * The second iteration --> optics version c Jan 2015 This de-squeeze is to 19/19/19/30 m and is based on HB's 90m
     * desqueeze for IR1+5. The tunes are rematched using the main quads RQD/RQF
     * 
     * @return
     */

    private OpticDefinitionSet create2015MediumBetaOpticsSetc() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 19 m in all IRs */

        builder.addOptic(
                "R2015c_A12mC12mA12mL12m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_12600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_12600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_12500_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_12500_951.madx") });

        builder.addOptic(
                "R2015c_A14mC14mA14mL14m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_14500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_14500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_14500_921.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_14500_951.madx") });

        builder.addOptic(
                "R2015c_A17mC17mA17mL17m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_16700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_16700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_17000_904.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_17000_951.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL19m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_19000_890.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        /* squeeze sequence down to 30 m in IR8 */

        builder.addOptic(
                "R2015c_A19mC19mA19mL21m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_21000_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL24m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_24000_856.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL27m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_27000_836.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL30m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_30000_816.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        return builder.build();
    }

    /**
     * The high beta set for 90 m - 'h'
     * 
     * @return
     */

    private OpticDefinitionSet create2015HighBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 90 m in all 1+5 */

        builder.addOptic(
                "R2015h_A12mC12mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_12600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_12600.madx") });

        builder.addOptic(
                "R2015h_A14mC14mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_14500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_14500.madx") });

        builder.addOptic(
                "R2015h_A17mC17mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_16700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_16700.madx") });

        builder.addOptic(
                "R2015h_A19mC19mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_19200.madx") });

        builder.addOptic(
                "R2015h_A22mC22mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_22000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_22000.madx") });

        builder.addOptic(
                "R2015h_A25mC25mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_25000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_25000.madx") });

        builder.addOptic(
                "R2015h_A30mC30mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_30000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_30000.madx") });

        builder.addOptic(
                "R2015h_A33mC33mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_33000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_33000.madx") });

        builder.addOptic(
                "R2015h_A36mC36mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_36000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_36000.madx") });

        builder.addOptic(
                "R2015h_A40mC40mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_40000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_40000.madx") });

        builder.addOptic(
                "R2015h_A43mC43mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_43000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_43000.madx") });

        builder.addOptic(
                "R2015h_A46mC46mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_46000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_46000.madx") });

        builder.addOptic(
                "R2015h_A51mC51mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_51000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_51000.madx") });

        builder.addOptic(
                "R2015h_A54mC54mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_54000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_54000.madx") });

        builder.addOptic(
                "R2015h_A60mC60mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_60000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_60000.madx") });

        builder.addOptic(
                "R2015h_A67mC67mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_67000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_67000.madx") });

        builder.addOptic(
                "R2015h_A75mC75mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_75000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_75000.madx") });

        builder.addOptic(
                "R2015h_A90mC90mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_90000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_90000.madx") });

        // a new version for the 90m
        
        builder.addOptic(
                "R2015h2_A90mC90mA10mL10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/v2/ir1_90000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/v2/ir5_90000.madx") });

        return builder.build();
    }
    /**
     * The ion optics set for tests with only IP2 squeezing
     * 
     * @return
     */

    private OpticDefinitionSet create2015IonOpticsTest1Set() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
         builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 90 m in all 1+5 */

        
        builder.addOptic(
                "R2015t1_A11mC11mA760L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA720L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_720.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA680L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_680.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA640L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_640.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA600L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_600.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA550L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_550.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA500L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_500.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA450L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_450.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA400L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_400.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA350L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA300L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_300.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA250L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA225L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_225.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA200L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA180L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_180.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA160L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_160.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA140L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_140.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA120L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_120.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA110L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_110.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA100L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_100.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA90L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_90.madx") });

        builder.addOptic(
                "R2015t1_A11mC11mA80L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_80.madx") });

        return builder.build();
    }

}