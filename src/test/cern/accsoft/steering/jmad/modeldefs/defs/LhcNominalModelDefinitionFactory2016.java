/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2016 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2016.seq", ModelFileLocation.REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2016");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2016";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2016 optics */
        definitionSetList.add(this.create2016InjectionOpticsSet());
        definitionSetList.add(this.create2016RandSLowBetaOpticsSet());
        definitionSetList.add(this.create2016LowBetaOpticsSet());
        definitionSetList.add(this.create2016MediumBetaRampAndSqueezeOpticsSet());
        definitionSetList.add(this.create2016MediumBetaOpticsSet());
        definitionSetList.add(this.create2016SpecialMdOpticsSet());
        definitionSetList.add(this.create2016AtsRampSqueezeOpticsSet());
        definitionSetList.add(this.create2016AtsSqueezeOpticsSet());
        definitionSetList.add(this.create2016AtsTeleSqueezeOpticsSet());
        definitionSetList.add(this.create2016HighBetaOpticsSet());
        definitionSetList.add(this.create2016HighBetaRampDesqueezeOpticsSet());
        definitionSetList.add(this.create2016IonRampAndSqueeze4TeVOpticsSet());
        definitionSetList.add(this.create2016IonQchange4TeVOpticsSet());
        definitionSetList.add(this.create2016IonRampAndSqueezeOpticsSet());
        definitionSetList.add(this.create2016TestIr8SqueezeIonOpticsSet());
        definitionSetList.add(this.create2016LowBetaIonOpticsSet());
                      
                  
        return definitionSetList;
    }

    private OpticDefinitionSet create2016InjectionOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic("R2016a_A11mC11mA10mL10m_INJ", new OpticModelFileBuilder[0]);

        return builder.build();
    }
    
    /**
     * For the combined R & Q at low beta - optics versions with tune @ injection values
     * @return
     */
    
    private OpticDefinitionSet create2016RandSLowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic(
                "R2016a_A900C900A10mL900_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A700C700A10mL800_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A400C400A10mL700_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A300C300A10mL600_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        return builder.build();
    }

 /**
  * The special optics for MD - alignement ...    
  * @return
  */
    private OpticDefinitionSet create2016SpecialMdOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
       builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj-balistic.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic("R2016a_A11mC11mA10mL10m_ALIGN",                
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/align/alignment_ir1.madx"),
                OpticModelFileBuilder.createInstance("strength/IR5/align/alignment_ir5.madx")} );

        return builder.build();
    }

    /**
     * The special optics for MD - ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)    
     * @return
     */
       private OpticDefinitionSet create2016AtsRampSqueezeOpticsSet() {
           OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

           /* initial optics strength files common to all optics (loaded before the other strength files) */
 //          builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/ats/ats_11m.madx"));

           /* final optics strength files common to all optics (loaded after the other strength files) */
           builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                   .parseAs(ParseType.STRENGTHS));
           builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                   .doNotParse());

           /* ramp and squeeze to 3m in 1,5 and 8 */
           builder.addOptic("R2016ats_A11mC11mA10mL10m", 
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_11m.madx")});
           
           builder.addOptic("R2016ats_A970C970A10mL970",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_970cm.madx")} );

           builder.addOptic("R2016ats_A920C920A10mL920",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_920cm.madx")} );

           builder.addOptic("R2016ats_A850C850A10mL850",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_850cm.madx")} );

           builder.addOptic("R2016ats_A760C760A10mL760",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_760cm.madx")} );

           builder.addOptic("R2016ats_A650C650A10mL650",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_650cm.madx")} );

           builder.addOptic("R2016ats_A550C550A10mL550",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_550cm.madx")} );

           builder.addOptic("R2016ats_A460C460A10mL460",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_460cm.madx")} );

           builder.addOptic("R2016ats_A380C380A10mL380",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_380cm.madx")} );

           builder.addOptic("R2016ats_A320C320A10mL320",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_320cm.madx")} );

           builder.addOptic("R2016ats_A300C300A10mL300",                
                   new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_300cm.madx")} );

           return builder.build();
       }

       /**
        * The special optics for MD - ATS squeeze 3m to 40 cm - presqueeze
        * @return
        */
       private OpticDefinitionSet create2016AtsSqueezeOpticsSet() {
              OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

              /* initial optics strength files common to all optics (loaded before the other strength files) */
//              builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/ats/ats_11m.madx"));

              /* final optics strength files common to all optics (loaded after the other strength files) */
              builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                      .parseAs(ParseType.STRENGTHS));
//              builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
//                      .doNotParse());

              /* ramp and squeeze to 3m in 1,5 and 8 */

              builder.addOptic("R2016ats_A220C220A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_220cm.madx")} );

              builder.addOptic("R2016ats_A160C160A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_160cm.madx")} );

              builder.addOptic("R2016ats_A120C120A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_120cm.madx")} );

              builder.addOptic("R2016ats_A90C90A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_90cm.madx")} );

              builder.addOptic("R2016ats_A70C70A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_70cm.madx")} );

              builder.addOptic("R2016ats_A55C55A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_55cm.madx")} );

              builder.addOptic("R2016ats_A45C45A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_45cm.madx")} );

              builder.addOptic("R2016ats_A40C40A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_40cm.madx")} );

              return builder.build();
          }
  
       /**
        * The special optics for MD - ATS squeeze from 37 cm 10 cm - tele style   
        * @return
        */
       private OpticDefinitionSet create2016AtsTeleSqueezeOpticsSet() {
              OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

              /* initial optics strength files common to all optics (loaded before the other strength files) */
//              builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/ats/ats_11m.madx"));

              /* final optics strength files common to all optics (loaded after the other strength files) */
              builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                      .parseAs(ParseType.STRENGTHS));
//              builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
//                      .doNotParse());

              /* ramp and squeeze to 3m in 1,5 and 8 */

              builder.addOptic("R2016ats_A37C37A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_37cm.madx")} );

              builder.addOptic("R2016ats_A33C33A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_33cm.madx")} );

              builder.addOptic("R2016ats_A27C27A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_27cm.madx")} );

              builder.addOptic("R2016ats_A21C21A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_21cm.madx")} );

              builder.addOptic("R2016ats_A17C17A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_17cm.madx")} );

              builder.addOptic("R2016ats_A14C14A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_14cm.madx")} );

              builder.addOptic("R2016ats_A12C12A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_12cm.madx")} );

              builder.addOptic("R2016ats_A10C10A10mL300",                
                      new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ats/ats_10cm.madx")} );

              return builder.build();
          }

       
    private OpticDefinitionSet create2016LowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());

        /* optic with coll tunes */
        builder.addOptic("R2016a_A11mC11mA10mL10m", new OpticModelFileBuilder[0]);

        /* squeeze sequence down to 0.8/10/0.08/3 */

        builder.addOptic(
                "R2016a_A900C900A10mL900",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A700C700A10mL800",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A400C400A10mL700",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A300C300A10mL600",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A250C250A10mL500",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A200C200A10mL400",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A150C150A10mL350",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A120C120A10mL325",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A100C100A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A90C90A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });

        builder.addOptic(
                "R2016a_A80C80A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_800_10000_800_3000.madx")
                        });
        builder.addOptic(
                "R2016a_A70C70A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_700_10000_700_3000.madx")
                        });
        builder.addOptic(
                "R2016a_A65C65A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_650_10000_650_3000.madx")
                        });
        builder.addOptic(
                "R2016a_A60C60A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_600_10000_600_3000.madx"),
                        });
        builder.addOptic(
                "R2016a_A55C55A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_550_10000_550_3000.madx")
                        });
        builder.addOptic(
                "R2016a_A50C50A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_500_10000_500_3000.madx"),
                        });
        builder.addOptic(
                "R2016a_A45C45A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_450_10000_450_3000.madx")
                    });
        builder.addOptic(
                "R2016a_A40C40A10mL300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_400_10000_400_3000.madx"),
                        });        


       /* down to 40 cm (new squeeze from Riccardo with phase advance)  */

        builder.addOptic(
                "R2016a_A70C70A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A65C65A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_650.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_650.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A60C60A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A55C55A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_550.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_550.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A50C50A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A45C45A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_450.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_450.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A43C43A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_430.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_430.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2016a_A40C40A10mL300_PA",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/PhaseAdvance/ir1_400.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/PhaseAdvance/ir5_400.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        

        /* down to 33 cm (new squeeze from Riccardo with optimized Q6 strengths)  */
        
        builder.addOptic(
                "R2017a_A334C334A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_3340_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_3340_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A208C208A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_2080_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_2080_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A153C153A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_1530_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_1530_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A124C124A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_1240_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_1240_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A91C91A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_910_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_910_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A70C70A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_700_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_700_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A56C56A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_560_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_560_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A46C46A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_460_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_460_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A39C39A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_390_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_390_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A36C36A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_360_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_360_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });
        builder.addOptic(
                "R2017a_A33C33A10mL300_Test",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/NewNominalTest/ir1_330_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/NewNominalTest/ir5_330_dev6.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951.madx") });


        
        return builder.build();
    }
 
    /**
     * The ion squeeze ALICE to 3m, LBCb to 1.5m, ATLAS & CMS to 0.6m
     * @return
     */
    
    private OpticDefinitionSet create2016LowBetaIonOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());


        /* squeeze sequence down to 0.6/2/0.6/1.5 */

        builder.addOptic(
                "R2016i2_A300C300A350L600",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });

        builder.addOptic(
                "R2016i2_A250C250A250L500",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });

        builder.addOptic(
                "R2016i2_A200C200A200L400",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });

        builder.addOptic(
                "R2016i2_A150C150A200L350",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });

        builder.addOptic(
                "R2016i2_A120C120A200L325",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });

        builder.addOptic(
                "R2016i2_A100C100A200L300",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });

        builder.addOptic(
                "R2016i2_A90C90A200L275",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2750_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });


        /* 40 cm (new squeeze from Riccardo with phase advance)  */

        builder.addOptic(
                "R2016i2_A80C80A200L250",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_800_10000_800_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2500_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx")
                        });
        builder.addOptic(
                "R2016i2_A70C70A200L200",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_700_10000_700_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2000_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx")
                        });
        builder.addOptic(
                "R2016i2_A65C65A200L175",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_650_10000_650_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_1750_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx")
                        });
        builder.addOptic(
                "R2016i2_A60C60A200L150",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/40cm/new/opt_600_10000_600_3000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_1500_875.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx")
                        });
        
        return builder.build();
    }
    


 
    /**
     * optics version c /2016 This de-squeeze is to 19/19/19/24 m and is based on HB's 90m
     * desqueeze for IR1+5. The tunes are rematched using the main quads RQD/RQF to injection tunes for the R&S
     * 
     * @return
     */

    private OpticDefinitionSet create2016MediumBetaRampAndSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 19 m in all IRs */

        builder.addOptic(
                "R2016c_A12mC12mA12mL12m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_12600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_12600.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_12500_934.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_12500_951.madx") });

        builder.addOptic(
                "R2016c_A14mC14mA14mL14m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_14500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_14500.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_14500_921.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_14500_951.madx") });

        builder.addOptic(
                "R2016c_A17mC17mA17mL17m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_16700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_16700.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_17000_904.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_17000_951.madx") });

        builder.addOptic(
                "R2016c_A19mC19mA19mL19m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_19000_890.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        /* squeeze sequence down to 30 m in IR8 */

        builder.addOptic(
                "R2016c_A19mC19mA19mL21m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_21000_877.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        builder.addOptic(
                "R2016c_A19mC19mA19mL24m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_24000_856.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        return builder.build();
    }
 
    /**
     * optics version c /2016 This de-squeeze is for 19/19/19/24 m and has only the collision tunes
     * 
     * @return
     */

    private OpticDefinitionSet create2016MediumBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());
 
        /* just have the last optics */

        builder.addOptic(
                "R2016c_A19mC19mA19mL24m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_24000_856.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951.madx") });

        return builder.build();
    }


    /**
     * Test for IR8 squeeze to 1.5m
     * 
     * @return
     */

    private OpticDefinitionSet create2016TestIr8SqueezeIonOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());
 
        /* just have the last optics */

        builder.addOptic(
                "R2016t1_A11mC11mA10mL300",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL275",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2750_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL250",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2500_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL225",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2250_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL200",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_2000_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL175",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_1750_875.madx") });
        builder.addOptic(
                "R2016t1_A11mC11mA10mL150",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_1500_875.madx") });

        return builder.build();
    }

    /**
     * Optics set for IR2 R&S to 4 TeV
     * 
     * @return
     */

    private OpticDefinitionSet create2016IonRampAndSqueeze4TeVOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 2m in IR2 */

        builder.addOptic(
                "R2016i1_A11mC11mA800L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_800.madx") });
        
        builder.addOptic(
                "R2016i1_A11mC11mA680L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_680.madx") });
 
        builder.addOptic(
                "R2016i1_A11mC11mA500L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_500.madx") });

        builder.addOptic(
                "R2016i1_A11mC11mA400L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_400.madx") });

        builder.addOptic(
                "R2016i1_A11mC11mA300L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_300.madx") });

        builder.addOptic(
                "R2016i1_A11mC11mA250L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });

        builder.addOptic(
                "R2016i1_A11mC11mA200L10m_INJ",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });


        return builder.build();
    }

    private OpticDefinitionSet create2016IonRampAndSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 19 m in all IRs */
    
        builder.addOptic(
            "R2016i2_A900C900A760L900_INJ",
            new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });


        builder.addOptic(
            "R2016i2_A700C700A720L800_INJ",
            new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_720.madx") });

        builder.addOptic(
            "R2016i2_A400C400A450L700_INJ",
            new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_450.madx") });

        builder.addOptic(
            "R2016i2_A300C300A350L600_INJ",
            new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895.madx"),
                    OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });

        return builder.build();
    }
   
    /**
     * Optics set for IR2 4 TeV Q change
     * 
     * @return
     */

    private OpticDefinitionSet create2016IonQchange4TeVOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 19 m in all IRs */

        builder.addOptic(
                "R2016i1_A11mC11mA200L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });


        return builder.build();
    }
 
    
    /**
     * The high beta set for 90 m - 'h'
     * 
     * @return
     */

    private OpticDefinitionSet create2016HighBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll.madx").isResource()
                .doNotParse());

 
        builder.addOptic(
                "R2016h_A60mC60mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_60000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_60000.madx") });

        builder.addOptic(
                "R2016h_A67mC67mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_67000.madx"),
                         OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_67000.madx") });

        builder.addOptic(
                "R2016h_A75mC75mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_75000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_75000.madx") });

        builder.addOptic(
                "R2016h_A82mC82mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_82000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_82000.madx") });

        builder.addOptic(
                "R2016h_A90mC90mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_90000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_90000.madx") });

        builder.addOptic(
                "R2016h_A105mC105mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0105.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0105.str") });

        builder.addOptic(
                "R2016h_A120mC120mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0120.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0120.str") });

        builder.addOptic(
                "R2016h_A140mC140mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0140.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0140.str") });

        builder.addOptic(
                "R2016h_A160mC160mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0160.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0160.str") });

        builder.addOptic(
                "R2016h_A180mC180mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0180.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0180.str") });

        builder.addOptic(
                "R2016h_A200mC200mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0200.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0200.str") });

        builder.addOptic(
                "R2016h_A230mC230mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0230.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0230.str") });

        builder.addOptic(
                "R2016h_A260mC260mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0260.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0260.str") });

        builder.addOptic(
                "R2016h_A300mC300mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0300.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0300.str") });

        builder.addOptic(
                "R2016h_A350mC350mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0350.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0350.str") });

        builder.addOptic(
                "R2016h_A400mC400mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0400.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0400.str") });

        builder.addOptic(
                "R2016h_A450mC450mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0450.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0450.str") });

        builder.addOptic(
                "R2016h_A500mC500mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0500.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0500.str") });

        builder.addOptic(
                "R2016h_A600mC600mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0600.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0600.str") });

        builder.addOptic(
                "R2016h_A700mC700mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0700.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0700.str") });

        builder.addOptic(
                "R2016h_A800mC800mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0800.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0800.str") });

        builder.addOptic(
                "R2016h_A900mC800mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_0900.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_0900.str") });

        builder.addOptic(
                "R2016h_A1000mC1000mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_1000.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_1000.str") });


        builder.addOptic(
                "R2016h_A1200mC1200mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_1200.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_1200.str") });

        builder.addOptic(
                "R2016h_A1500mC1500mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_1500.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_1500.str") });

        builder.addOptic(
                "R2016h_A1700mC1700mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_1700.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_1700.str") });

        builder.addOptic(
                "R2016h_A2000mC2000mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2000.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2000.str") });

        builder.addOptic(
                "R2016h_A2200mC2200mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2200.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2200.str") });

        builder.addOptic(
                "R2016h_A2400mC2400mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2400.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2400.str") });

        builder.addOptic(
                "R2016h_A2500mC2500mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2500.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2500.str") });
       
        builder.addOptic(
                "R2016h_A2500mC2500mA10mL10m_IR1_1",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2500_1.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2500.str") });
        
        builder.addOptic(
                "R2016h_A2500mC2500mA10mL10m_IR1_2",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2500_2.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2500.str") });
        
        builder.addOptic(
                "R2016h_A2500mC2500mA10mL10m_IR1_3",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2500_3.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2500.str") });
        
        builder.addOptic(
                "R2016h_A2500mC2500mA10mL10m_IR1_4",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/IP1_2500_4.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/IP5_2500.str") });

        return builder.build();
    }

    /**
     * The high beta set for 60 m - 'h' --> at injection tune for the ramp - desqueeze
     * 
     * @return
     */

    private OpticDefinitionSet create2016HighBetaRampDesqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 90 m in all 1+5 */

        builder.addOptic(
                "R2016h_A12mC12mA10mL10m_INJ",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_12000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_12000.madx") });

        builder.addOptic(
                "R2016h_A14mC14mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_14000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_14000.madx") });

        builder.addOptic(
                "R2016h_A17mC16mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_16000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_16000.madx") });

        builder.addOptic(
                "R2016h_A19mC19mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_19000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_19000.madx") });

        builder.addOptic(
                "R2016h_A22mC22mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_22000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_22000.madx") });

        builder.addOptic(
                "R2016h_A25mC25mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_25000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_25000.madx") });

        builder.addOptic(
                "R2016h_A30mC30mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_30000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_30000.madx") });

        builder.addOptic(
                "R2016h_A36mC36mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_36000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_36000.madx") });

        builder.addOptic(
                "R2016h_A43mC43mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_43000.madx"),
                         OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_43000.madx") });

        builder.addOptic(
                "R2016h_A51mC51mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_51000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_51000.madx") });


        builder.addOptic(
                "R2016h_A60mC60mA10mL10m_INJ",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_60000.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_60000.madx") });

 
        return builder.build();
    }
    /**
     * The ion optics set for tests with only IP2 squeezing
     * 
     * @return
     */

    private OpticDefinitionSet create2015IonOpticsTest1Set() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
         builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 90 m in all 1+5 */

        
        builder.addOptic(
                "R2015t1_A11mC11mA760L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_760.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA720L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_720.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA680L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_680.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA640L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_640.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA600L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_600.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA550L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_550.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA500L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_500.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA450L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_450.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA400L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_400.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA350L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_350.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA300L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_300.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA250L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_250.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA225L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_225.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA200L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_200.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA180L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_180.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA160L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_160.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA140L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_140.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA120L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_120.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA110L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_110.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA100L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_100.madx") });
        
        builder.addOptic(
                "R2015t1_A11mC11mA90L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_90.madx") });

        builder.addOptic(
                "R2015t1_A11mC11mA80L10m",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR2/ions/ir2_80.madx") });

        return builder.build();
    }

}