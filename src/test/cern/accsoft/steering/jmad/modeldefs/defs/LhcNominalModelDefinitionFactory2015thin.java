/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2015thin extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2015.seq", ModelFileLocation.REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2015/V6.503");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2015 THIN";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2014 optics */
        definitionSetList.add(this.create2015InjectionOpticsSet());
        definitionSetList.add(this.create2015LowBetaOpticsSet());
        definitionSetList.add(this.create2015MediumBetaOpticsSetc());
        definitionSetList.add(this.create2015HighBetaOpticsSeth());
        return definitionSetList;
    }

    private OpticDefinitionSet create2015InjectionOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("slice.madx").isResource().doNotParse());
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes_thin.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource().parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource().doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource().doNotParse());

        /* injection optics */
        builder.addOptic("R2015a_A11mC11mA10mL10m_INJ_thin", new OpticModelFileBuilder[0]);

        return builder.build();
    }

    private OpticDefinitionSet create2015LowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("slice.madx").isResource().doNotParse());
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes_thin.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* optic with coll tunes */
        builder.addOptic("R2015a_A11mC11mA10mL10m_thin", new OpticModelFileBuilder[0]);

        /* squeeze sequence down to 0.8/10/0.08/3 */

        builder.addOptic(
                "R2015a_A900C900A10m_0.00950L900_0.00934_thin",
                new OpticModelFileBuilder[] 
                  { 
                        OpticModelFileBuilder.createInstance("strength/IR1/ir1_9000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_9000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_9000_934_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") 
                  });

        builder.addOptic(
                "R2015a_A700C700A10m_0.00950L800_0.00919_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_7000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_7000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_8000_919_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A400C400A10m_0.00950L700_0.00906_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_4000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_4000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_7000_906_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A300C300A10m_0.00950L600_0.00895_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_3000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_3000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_6000_895_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A250C250A10m_0.00950L500_0.00886_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_5000_886_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A200C200A10m_0.00950L400_0.00880_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_2000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_2000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_4000_880_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A150C150A10m_0.00950L350_0.00877_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3500_877_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A120C120A10m_0.00950L325_0.00876_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3250_876_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A100C100A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_1000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_1000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A90C90A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_900_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_900_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        builder.addOptic(
                "R2015a_A80C80A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_800_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_800_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        /* down to 40 cm */

        builder.addOptic(
                "R2015a_A70C70A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_700_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_700_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A65C65A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_650_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_650_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A60C60A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_600_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_600_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A55C55A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_550_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_550_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A50C50A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A45C45A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_450_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_450_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });
        builder.addOptic(
                "R2015a_A40C40A10m_0.00950L300_0.00875_thin",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/ir1_400_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/ir5_400_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/ir8_3000_875_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/ir2_10000_951_thin.madx") });

        return builder.build();
    }

//    private OpticDefinitionSet create2015MediumBetaOpticsSetb() {
//        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();
//
//        /* initial optics strength files common to all optics (loaded before the other strength files) */
//        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("slice.madx").isResource().doNotParse());
//        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes_thin.madx"));
//
//        /* final optics strength files common to all optics (loaded after the other strength files) */
//        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
//                .parseAs(ParseType.STRENGTHS));
//        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
//                .doNotParse());
//
//        /* squeeze sequence down to 20/20/20/20 */
//
//        builder.addOptic(
//                "R2015b_A12mC12mA12mL12m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_12000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_12000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_12000_937_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_12000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A14mC14mA14mL14m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_14000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_14000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_14000_923_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_14000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A16mC16mA16mL16m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_16000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_16000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_16000_910_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_16000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A18mC18mA18mL18m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_18000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_18000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_18000_896_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_18000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A20mC20mA20mL20m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_20000_883_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950_thin.madx") });
//
//        /* squeeze sequence down to 30 m in IR8 */
//
//        builder.addOptic(
//                "R2015b_A20mC20mA20mL22m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_22000_869_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A20mC20mA20mL26m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_26000_842_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950_thin.madx") });
//
//        builder.addOptic(
//                "R2015b_A20mC20mA20mL30m_thin",
//                new OpticModelFileBuilder[] {
//                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_20000_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_30000_815_thin.madx"),
//                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_20000_950_thin.madx") });
//
//        return builder.build();
//    }

    /**
     * The second iteration --> optics version c Jan 2015 This de-squeeze is to 19/19/19/30 m and is based on HB's 90m
     * desqueeze for IR1+5. The tunes are rematched using the main quads RQD/RQF
     * 
     * @return
     */

    private OpticDefinitionSet create2015MediumBetaOpticsSetc() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("slice.madx").isResource().doNotParse());
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes_thin.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 19 m in all IRs */

        builder.addOptic(
                "R2015c_A12mC12mA12mL12m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_12600_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_12600_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_12500_934_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_12500_951_thin.madx") });

        builder.addOptic(
                "R2015c_A14mC14mA14mL14m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_14500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_14500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_14500_921_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_14500_951_thin.madx") });

        builder.addOptic(
                "R2015c_A17mC17mA17mL17m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_16700_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_16700_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_17000_904_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_17000_951_thin.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL19m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_19000_890_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951_thin.madx") });

        /* squeeze sequence down to 30 m in IR8 */

        builder.addOptic(
                "R2015c_A19mC19mA19mL21m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_21000_877_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951_thin.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL24m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_24000_856_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951_thin.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL27m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_27000_836_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951_thin.madx") });

        builder.addOptic(
                "R2015c_A19mC19mA19mL30m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betamed/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betamed/ir5_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR8/betamed/ir8_30000_816_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR2/betamed/ir2_19000_951_thin.madx") });

        return builder.build();
    }

    /**
     * The high beta set for 90 m - 'h'
     * 
     * @return
     */

    private OpticDefinitionSet create2015HighBetaOpticsSeth() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("slice.madx").isResource().doNotParse());
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes_thin.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-col-rq.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* squeeze sequence down to 90 m in all 1+5 */

        builder.addOptic(
                "R2015h_A12mC12mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_12600_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_12600_thin.madx") });

        builder.addOptic(
                "R2015h_A14mC14mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_14500_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_14500_thin.madx") });

        builder.addOptic(
                "R2015h_A17mC17mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_16700_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_16700_thin.madx") });

        builder.addOptic(
                "R2015h_A19mC19mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_19200_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_19200_thin.madx") });

        builder.addOptic(
                "R2015h_A22mC22mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_22000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_22000_thin.madx") });

        builder.addOptic(
                "R2015h_A25mC25mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_25000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_25000_thin.madx") });

        builder.addOptic(
                "R2015h_A30mC30mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_30000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_30000_thin.madx") });

        builder.addOptic(
                "R2015h_A33mC33mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_33000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_33000_thin.madx") });

        builder.addOptic(
                "R2015h_A36mC36mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_36000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_36000_thin.madx") });

        builder.addOptic(
                "R2015h_A40mC40mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_40000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_40000_thin.madx") });

        builder.addOptic(
                "R2015h_A43mC43mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_43000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_43000_thin.madx") });

        builder.addOptic(
                "R2015h_A46mC46mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_46000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_46000_thin.madx") });

        builder.addOptic(
                "R2015h_A51mC51mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_51000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_51000_thin.madx") });

        builder.addOptic(
                "R2015h_A54mC54mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_54000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_54000_thin.madx") });

        builder.addOptic(
                "R2015h_A60mC60mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_60000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_60000_thin.madx") });

        builder.addOptic(
                "R2015h_A67mC67mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_67000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_67000_thin.madx") });

        builder.addOptic(
                "R2015h_A75mC75mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_75000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_75000_thin.madx") });

        builder.addOptic(
                "R2015h_A90mC90mA10mL10m_thin",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/betahigh/ir1_90000_thin.madx"),
                        OpticModelFileBuilder.createInstance("strength/IR5/betahigh/ir5_90000_thin.madx") });

        // a new version for the 90m
        //builder.addOptic(
        //        "R2015h2_A90mC90mA10mL10m_thin",
        //        new OpticModelFileBuilder[] {
        //                OpticModelFileBuilder.createInstance("strength/IR1/betahigh/v2/ir1_90000_thin.madx"),
        //                OpticModelFileBuilder.createInstance("strength/IR5/betahigh/v2/ir5_90000_thin.madx") });

       
        return builder.build();
    }

}
