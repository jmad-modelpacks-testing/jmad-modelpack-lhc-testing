/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2014 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2014.seq", ModelFileLocation.REPOSITORY));
   }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        //offsets.setResourceOffset("2014");
        offsets.setRepositoryOffset("2014/V6.503");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2014";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2014 optics */
        definitionSetList.add(this.create2014NominalOpticsSet());
        return definitionSetList;
    }


    private OpticDefinitionSet create2014NominalOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/V6.5.inj_special.str"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma.madx").isResource()
                .doNotParse());

        /* injection optics */
        builder.addOptic("A1100C1100A1000L1000_INJ_2014", new OpticModelFileBuilder[0]);

        /* Tune shift Optic */
        builder.addOptic("A1100C1100A1000L1000_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta11.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta11.00.str") });

        /* squeeze sequence */
        builder.addOptic(
                "A900C900A900_0.00915L750_0.00932_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_9.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_9.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00932_beta7.50m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00915_beta9.00m.str") });
        builder.addOptic(
                "A700C700A750_0.00897L600_0.00909_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_7.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_7.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00909_beta6.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00897_beta7.50m.str") });
        builder.addOptic(
                "A400C400A600_0.00889L500_0.00900_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_4.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_4.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00900_beta5.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta6.00m.str") });
        builder.addOptic(
                "A300C300A500_0.00889L375_0.00888_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("IR1/IP1_beta_3.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_3.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00888_beta3.75m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta5.00m.str") });
        builder.addOptic(
                "A250C250A450_0.00889L350_0.00882_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("IR1/IP1_beta_2.50.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_2.50.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00882_beta3.50m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta4.50m.str") });
        builder.addOptic(
                "A200C200A400_0.00889L325_0.00878_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_2.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_2.00.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00878_beta3.25m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta4.00m.str") });
        builder.addOptic(
                "A160C160A350_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_1.60.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_1.60.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.50m.str") });
        builder.addOptic(
                "A150C150A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/IR1/IP1_beta_1.50.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/IP5_beta_1.50.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
       builder.addOptic(
                "A120C120A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_1.20m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_1.20m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A100C100A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_1.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_1.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A90C90A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.90m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.90m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A80C80A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.80m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.80m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A70C70A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.70m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.70m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A65C65A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.65m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.65m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A60C60A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.60m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.60m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        builder.addOptic(
                "A55C55A300_0.00889L300_0.00875_2014",
                new OpticModelFileBuilder[] {
                        OpticModelFileBuilder.createInstance("strength/IR1/new_ip1_b2_squeeze/IP1_beta_0.55m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR5/new_ip5_b2_squeeze/IP5_beta_0.55m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR8/3.5TeV/special/ip8_0.00875_beta3.00m.str"),
                        OpticModelFileBuilder.createInstance("strength/IR2/3.5TeV/special/ip2_0.00889_beta3.00m.str") });
        
        return builder.build();
    }

}