/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.REPOSITORY;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2017 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2017.seq", REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2017");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2017";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2017 optics */
        definitionSetList.add(this.create2017AtsLowBetaRampSqueezeOpticsSet());
        definitionSetList.add(this.create2017Balistic());
        definitionSetList.add(this.create2017AtsLowBetaTelescopicSqueeze());
        definitionSetList.add(this.create2017AtsFlatTelescopicSqueeze());
        definitionSetList.add(this.create2017HighBetaTestAtInjection());
        definitionSetList.add(this.create2017HighBetaAtInjection());
        definitionSetList.add(this.createHalfIntegerIjectionOptics());

        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     * 
     * @return
     */
    private OpticDefinitionSet create2017AtsLowBetaRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        // builder.addFinalCommonOpticFile(
        // OpticModelFileBuilder.createInstance("match-chroma.madx").isResource().doNotParse());

        /* ramp and squeeze to 3m in 1,5 and 8 */
        builder.addOptic("R2017a_A11mC11mA10mL10m", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_11m_fixQ8L4.madx") });

        builder.addOptic("R2017a_A970C970A10mL970", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_970cm.madx") });

        builder.addOptic("R2017a_A920C920A10mL920", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_920cm.madx") });

        builder.addOptic("R2017a_A850C850A10mL850", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_850cm.madx") });

        builder.addOptic("R2017a_A740C740A10mL740", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_740cm.madx") });

        builder.addOptic("R2017a_A630C630A10mL630", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_630cm.madx") });

        builder.addOptic("R2017a_A530C530A10mL530", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_530cm.madx") });

        builder.addOptic("R2017a_A440C440A10mL440", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_440cm.madx") });

        builder.addOptic("R2017a_A360C360A10mL360", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_360cm.madx") });

        builder.addOptic("R2017a_A310C310A10mL310", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_310cm.madx") });

        builder.addOptic("R2017a_A230C230A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_230cm.madx") });

        builder.addOptic("R2017a_A180C180A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_180cm.madx") });

        builder.addOptic("R2017a_A135C135A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_135cm.madx") });

        builder.addOptic("R2017a_A100C100A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_100cm.madx") });

        builder.addOptic("R2017a_A80C80A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_80cm.madx") });

        builder.addOptic("R2017a_A65C65A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_65cm.madx") });

        builder.addOptic("R2017a_A54C54A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_54cm.madx") });

        builder.addOptic("R2017a_A46C46A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_46cm.madx") });

        builder.addOptic("R2017a_A40C40A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_40cm.madx") });

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_40cm_ctpps1.madx") });

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_40cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A37C37A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_37cm.madx") });

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_37cm_ctpps1.madx") });

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_37cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A33C33A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_33cm.madx") });

        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_33cm_ctpps1.madx") });

        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_33cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A30C30A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_30cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A27C27A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_27cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A25C25A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_25cm_ctpps2.madx") });

        return builder.build();
    }

        
    /**
     * Special optics for MD, all squeeze telescopic - All at collision tune --> trimmed with knob to INJ (BP level)
     * 
     * @return
     */
    private OpticDefinitionSet create2017AtsLowBetaTelescopicSqueeze() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));

        /* Telescopic squeeze from 1 m to 25 cm (MD) */

        builder.addOptic("R2017aT100_A92C92A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_92cm.madx") });

        builder.addOptic("R2017aT100_A80C80A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_80cm.madx") });

        builder.addOptic("R2017aT100_A65C65A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_65cm.madx") });

        builder.addOptic("R2017aT100_A50C50A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_50cm.madx") });

        builder.addOptic("R2017aT100_A40C40A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_40cm.madx") });

        builder.addOptic("R2017aT100_A35C35A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_35cm.madx") });

        builder.addOptic("R2017aT100_A30C30A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_30cm.madx") });

        builder.addOptic("R2017aT100_A25C25A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/telescopic/ats_tele_25cm.madx") });

        return builder.build();
    }

    
    /**
     * Special FLAT optics for MD, all squeeze telescopic - All at collision tune --> trimmed with knob to INJ (BP level)
     * 
     * @return
     */
    private OpticDefinitionSet create2017AtsFlatTelescopicSqueeze() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));

        builder.addOptic("R2017aT65_A60_51C51_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-51cm.madx") });

        builder.addOptic("R2017aT65_A60_41C41_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-41cm.madx") });

        builder.addOptic("R2017aT65_A60_31C31_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-31cm.madx") });

        builder.addOptic("R2017aT65_A60_21C21_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-21cm.madx") });

        builder.addOptic("R2017aT65_A60_15C15_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-15cm.madx") });

        builder.addOptic("R2017aT65_A60_12C12_60A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele60-12_5cm.madx") });

        builder.addOptic("R2017aT65_A47_12C12_47A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele47-12cm.madx") });

        builder.addOptic("R2017aT65_A36_12C12_36A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele36-12cm.madx") });

        builder.addOptic("R2017aT65_A30_12C12_30A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/flatMD/ats_flattele30-12cm.madx") });

        return builder.build();
    }

    /**
     * Special Injection optics for half integer MD
     * 
     * @return
     */
    private OpticDefinitionSet createHalfIntegerIjectionOptics() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));

        builder.addOptic("R2017a_A11mC11mA10mL10m_Q62_41-60_43", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/HalfIntegerMD/ats_11m_62.41_60.43.madx") });

        return builder.build();
    }

    
    /**
     * Optics for highbeta at injection
     * 
     * @return
     */
    private OpticDefinitionSet create2017HighBetaTestAtInjection() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/opt_inj_colltunes.madx"));
        
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma-ats.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-inj.madx").isResource()
                .doNotParse());
        
        builder.addOptic("R2017h_A51mC51mA10mL10m", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/highbeta/IP1_0051_2.madx"),
                OpticModelFileBuilder.createInstance("strength/highbeta/IP5_0051_2.madx") });
        
        return builder.build();
    }
    
    private OpticDefinitionSet create2017HighBetaAtInjection() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/ats/low-beta/V2/ats_11m_fixQ8L4.madx"));
        
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll-rq-ats.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma-ats.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll-ats.madx").isResource()
                .doNotParse());
               
        builder.addOptic("R2017h_A100mC100mA10mL10m", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/highbeta/IP1_0100_v4.madx"),
                OpticModelFileBuilder.createInstance("strength/highbeta/IP5_0100_v3b.madx"),
                OpticModelFileBuilder.createInstance("strength/highbeta/FixTune_0100_v3b4.madx")});

        return builder.build();
    }
    
    private OpticDefinitionSet create2017Balistic() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        builder.addInitialCommonOpticFile(
                OpticModelFileBuilder.createInstance("strength/ballistic/opt_inj_colltunes2016.madx"));

        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        // builder.addFinalCommonOpticFile(
        // OpticModelFileBuilder.createInstance("match-chroma.madx").isResource().doNotParse());

        builder.addOptic("R2017b_A11mC11mA10mL10m",
                new OpticModelFileBuilder[] { OpticModelFileBuilder.createInstance("strength/ballistic/ir1_align.madx"),
                        OpticModelFileBuilder.createInstance("strength/ballistic/ir5_align.madx") });

        return builder.build();
    }

}