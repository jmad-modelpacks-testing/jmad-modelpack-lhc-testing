/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.REPOSITORY;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.beam.Beam;
import cern.accsoft.steering.jmad.domain.beam.Beam.Direction;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.MadxRange;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinition;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinition;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.modeldefs.LhcUtil;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcB4ModelDefinitionFactory2017 extends LhcNominalModelDefinitionFactory2017 {

    @Override
    protected void callback(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.removeInitFile(new CallableModelFileImpl("sequence/lhc2017.seq", REPOSITORY));

        modelDefinition.addInitFile(new CallableModelFileImpl("2017/sequence/lhcb4_study_20150119.seq", RESOURCE));
        modelDefinition.removeSequenceDefinition("lhcb1");
        modelDefinition.removeSequenceDefinition("lhcb2");

        SequenceDefinition b4sequence = createB4Sequence();
        modelDefinition.addSequenceDefinition(b4sequence);
        modelDefinition.setDefaultSequenceDefinition(b4sequence);
    }

    private SequenceDefinition createB4Sequence() {
        Beam beam4 = createBeam(Direction.PLUS);

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl lhcb4 = new SequenceDefinitionImpl("lhcb2", beam4);

        RangeDefinition allRangeB4 = createB4Range(lhcb4, LhcUtil.DEFAULT_RANGE_NAME, "#s", "#e", createDefaultTwiss());
        lhcb4.addRangeDefinition(allRangeB4);
        lhcb4.setDefaultRangeDefinition(allRangeB4);

        return lhcb4;
    }

    private final RangeDefinitionImpl createB4Range(SequenceDefinition sequenceDefinition, String name, String start,
            String end, TwissInitialConditionsImpl twiss) {
        RangeDefinitionImpl rangeDefinition = new RangeDefinitionImpl(sequenceDefinition, name,
                new MadxRange(start, end), twiss);
        addPostUseFiles(rangeDefinition);
        return rangeDefinition;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2017 B4";
    }

}